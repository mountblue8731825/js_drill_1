let inventory = require("./inventory");

function getBmwAndAudi(inventory) {
  let carsInfo = [];
  for (let i = 0; i < inventory.length; i++) {
    if (
      inventory[i].car_make.toLowerCase() === "audi" ||
      inventory[i].car_make.toLowerCase() === "bmw"
    ) {
      carsInfo.push(inventory[i]);
    }
  }
  return carsInfo;
}

module.exports = getBmwAndAudi;
