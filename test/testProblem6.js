const testInventory = require("./testInventory");
const problem6 = require("../problem6");

const testProblem6 = (problem, inventory) => {
  const result = problem(inventory);
  return result;
};

console.log(testProblem6(problem6, testInventory));

module.exports = testProblem6;
