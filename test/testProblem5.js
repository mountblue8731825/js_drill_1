const testInventory = require("./testInventory");
const problem5 = require("../problem5");

const testProblem5 = (problem, inventory) => {
  const result = problem(inventory);
  return result;
};

console.log(testProblem5(problem5, testInventory));

module.exports = testProblem5;
