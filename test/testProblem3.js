const testInventory = require("./testInventory");
const problem3 = require("../problem3");

const testProblem3 = (problem, inventory) => {
  const result = problem(inventory);
  return result;
};

console.log(testProblem3(problem3, testInventory));

module.exports = testProblem3;
