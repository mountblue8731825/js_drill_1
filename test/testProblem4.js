const testInventory = require("./testInventory");
const problem4 = require("../problem4");

const testProblem4 = (problem, inventory) => {
  const result = problem(inventory);
  return result;
};

console.log(testProblem4(problem4, testInventory));

module.exports = testProblem4;
