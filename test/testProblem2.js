const problem2 = require("../problem2");
const inventory = require("./testInventory");

const testProblem2 = (problem, inventory) => {
  const result = problem(inventory);
  return result;
};

console.log(testProblem2(problem2, inventory));

module.exports = testProblem2;
