let problem1 = require("../problem1");
let inventory = require("./testInventory");

const testProblem1 = (problem, inventory) => {
  const result = problem(inventory, 8);
  return result;
};

console.log(testProblem1(problem1, inventory));

module.exports = testProblem1;
