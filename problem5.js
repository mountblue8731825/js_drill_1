let inventory = require("./inventory");
let getCarYears = require("./problem4");

function getCarsAbove(inventory) {
  let carYears = getCarYears(inventory);
  let carsInfo = [];
  for (let i = 0; i < carYears.length - 1; i++) {
    if (carYears[i] < 2000) {
      carsInfo.push(inventory[i]);
    }
  }
  return carsInfo;
}

module.exports = getCarsAbove;
