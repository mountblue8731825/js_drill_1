let inventory = require("./inventory");

function sortByModel(inventory) {
  let copyInventory = [...inventory];
  copyInventory.sort((a, b) => {
    if (a.car_model.toLowerCase() < b.car_model.toLowerCase()) {
      return -1;
    }
    if (a.car_model.toLowerCase() > b.car_model.toLowerCase()) {
      return 1;
    }
    return 0;
  });
  return copyInventory;
}

module.exports = sortByModel;
