let inventory = require("./inventory");

function getInfoWithId(inventory, id) {
  var carInfo = "";
  for (let i = 0; i < inventory.length; i++) {
    if (id === inventory[i].id) {
      carInfo = inventory[i];
      break;
    }
  }
  return `Car ${id} is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`;
}

module.exports = getInfoWithId;
